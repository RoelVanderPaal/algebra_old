First (abandoned) attempt at implementing http://deeplearning.net/software/theano/ in Scala. Improved version (with symbolic differentiation) coming soon!

This project is provided as is. JCublas, JCuda and JCudaVec (and corresponding DLLs/SOs/DYLIBs) should be added to the classpath in order to run the GPU test.


# License
MIT License