package scala.algebra.graph

import breeze.linalg.DenseVector
import scala.algebra.graph.AlgebraDouble._
import breeze.linalg.DenseMatrix

class CPUEvaluator extends Evaluator {
  import breeze.numerics._
  def eval[U <: Tensor[Double]](n: Node[U], values: Values): U = {
    def getScalar(v: Node[Scalar[Double]]) = eval(v, values).data
    def getVector(v: Node[Vector[Double]]) = DenseVector(eval(v, values).data)

    n match {
      case v: VariableScalar => values.get(v).get
      case v: VariableVector => values.get(v).get
      case v: VariableMatrix => values.get(v).get
      case unOp: UnaryOperationScalar => {
        val value = getScalar(unOp.v)
        Scalar(Array(unOp.op match {
          case SinOp => Math.sin(value(0))
          case CosOp => Math.cos(value(0))
          case ExpOp => Math.exp(value(0))
        }))
      }
      case unOp: UnaryOperationVector => {
        val value = getVector(unOp.v)
        Vector((unOp.op match {
          case SinOp => breeze.numerics.sin(value)
          case CosOp => breeze.numerics.cos(value)
          case ExpOp => breeze.numerics.exp(value)
        }).data)
      }
      case unOp: UnaryOperationMatrix => {
        val matrix = eval(unOp.v, values)
        val value = new DenseMatrix(matrix.rows, matrix.cols, matrix.data)
        Matrix((unOp.op match {
          case SinOp => breeze.numerics.sin(value)
          case CosOp => breeze.numerics.cos(value)
          case ExpOp => breeze.numerics.exp(value)
        }).data, matrix.rows, matrix.cols)
      }
      case binOp: BinaryOperationScalarScalar => {
        val v1Val = getScalar(binOp.v1)
        val v2Val = getScalar(binOp.v2)
        Scalar(Array(binOp.op match {
          case AddOp => v1Val(0) + v2Val(0)
          case MulOp => v1Val(0) * v2Val(0)
        }))
      }
      case binOp: BinaryOperationScalarVector => {
        val v1Val = getScalar(binOp.v1)
        val v2Val = getVector(binOp.v2)
        Vector((binOp.op match {
          case AddOp => v2Val + v1Val(0)
          case MulOp => v2Val * v1Val(0)
        }).data)
      }
      case binOp: BinaryOperationVectorScalar => {
        val v1Val = getVector(binOp.v1)
        val v2Val = getScalar(binOp.v2)
        Vector((binOp.op match {
          case AddOp => v1Val + v2Val(0)
          case MulOp => v1Val * v2Val(0)
        }).data)
      }
      case binOp: BinaryOperationVectorVector => {
        val v1Val = getVector(binOp.v1)
        val v2Val = getVector(binOp.v2)
        Vector((binOp.op match {
          case AddOp => v1Val + v2Val
          case MulOp => throw new UnsupportedOperationException
        }).data)
      }
      case binOp: BinaryOperationMatrixScalar => {
        val matrix = eval(binOp.v1, values)
        val v1Val = new DenseMatrix(matrix.rows, matrix.cols, matrix.data)
        val v2Val = getScalar(binOp.v2)
        Matrix((binOp.op match {
          case AddOp => v1Val + v2Val(0)
          case MulOp => v1Val * v2Val(0)
        }).data, matrix.rows, matrix.cols)
      }
      case binOp: BinaryOperationMatrixVector => {
        val matrix = eval(binOp.v1, values)
        val v1Val = new DenseMatrix(matrix.rows, matrix.cols, matrix.data)
        val v2Val = getVector(binOp.v2)
        Vector((binOp.op match {
          case AddOp => throw new UnsupportedOperationException
          case MulOp => v1Val * v2Val
        }).data)
      }
      case binOp: BinaryOperationMatrixMatrix => {
        val matrix1 = eval(binOp.v1, values)
        val v1Val = new DenseMatrix(matrix1.rows, matrix1.cols, matrix1.data)
        val matrix2 = eval(binOp.v2, values)
        val v2Val = new DenseMatrix(matrix2.rows, matrix2.cols, matrix2.data)
        val result = Matrix((binOp.op match {
          case AddOp => v1Val + v2Val
          case MulOp => v1Val * v2Val
        }).data, matrix1.rows, matrix2.cols)
        result
      }
      case _ => throw new UnsupportedOperationException
    }
  }
}
