package scala.algebra.graph

import scala.language.existentials
import scala.reflect.runtime.universe._
import scala.algebra.graph.AlgebraDouble._
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import jcuda.Pointer
import jcuda._
import jcuda.driver.JCudaDriver._
import jcuda.runtime.JCuda._
import jcuda.jcublas.JCublas2._
import jcuda.jcublas.cublasHandle
import jcuda.jcublas.cublasOperation._
import jcuda.vec.VecDouble
import jcuda.vec.VecFloat

class GPUEvaluator extends Evaluator {
  case class SizedPointer(pointer: Pointer, rows: Int, cols: Int)

  def eval[U <: Tensor[Double]](n: Node[U], values: Values): U = {
    val handle = new cublasHandle()
    val oneScalar = Pointer.to(Array[Double](1))
    val zeroScalar = Pointer.to(Array[Double](0))
    cublasCreate(handle)
    VecDouble.init()

    val pointerMap: Map[Node[_], SizedPointer] = Map(values.data.map { v =>
      {
        def createVectorPointer(data: Array[_], rows: Int, cols: Int): SizedPointer = {
          val pointer = new Pointer()
          cudaMalloc(pointer, (rows * cols) * Sizeof.DOUBLE)
          cublasSetVector(rows * cols, Sizeof.DOUBLE, Pointer.to(data.asInstanceOf[Array[Double]]), 1, pointer, 1)
          SizedPointer(pointer, rows, cols)
        }

        val sizedPointer = v.v match {
          case s: Scalar[_] => createVectorPointer(s.data, 1, 1)
          case v: Vector[_] => createVectorPointer(v.data, v.data.length, 1)
          case m: Matrix[_] => createVectorPointer(m.data, m.rows, m.cols)
        }
        v.k -> sizedPointer
      }
    }: _*)

    def executeUnaryOperation[U <: Tensor[Double]](unOp: UnaryOperation[U]) = {
      val sp = unOp.v match {
        case v: Variable[_] => pointerMap.get(unOp.v).get
        case b: BinaryOperation[_, _, _] => eval(b)
        case u: UnaryOperation[_] => eval(u)
      }
      val result = new Pointer()
      cudaMalloc(result, sp.rows * sp.cols * Sizeof.DOUBLE)
      val operation = unOp.op match {
        case SinOp => VecDouble.sin(_, _, _)
        case CosOp => VecDouble.cos(_, _, _)
        case ExpOp => VecDouble.exp(_, _, _)
      }
      operation(sp.rows * sp.cols, result, sp.pointer)
      SizedPointer(result, sp.rows, sp.cols)
    }
    def eval[U <: Tensor[Double]](n: Node[U]): SizedPointer = n match {
      case v: Variable[_] => pointerMap.get(v).get
      case unOp: UnaryOperationVector => executeUnaryOperation(unOp)
      case unOp: UnaryOperationMatrix => executeUnaryOperation(unOp)
      case binOp: BinaryOperationMatrixVector => {
        val sp1 = pointerMap.get(binOp.v1).get
        val sp2 = pointerMap.get(binOp.v2).get
        val result = new Pointer()
        cudaMalloc(result, sp2.rows * sp2.cols * Sizeof.DOUBLE)
        cublasDgemv(handle, CUBLAS_OP_N, sp1.rows, sp1.cols, oneScalar, sp1.pointer, sp1.rows,
          sp2.pointer, sp2.cols, zeroScalar, result, sp2.cols)
        SizedPointer(result, sp1.rows, sp2.cols)
      }
      case binOp: BinaryOperationMatrixMatrix => {
        val sp1 = pointerMap.get(binOp.v1).get
        val sp2 = pointerMap.get(binOp.v2).get
        val result = new Pointer()
        cudaMalloc(result, sp1.rows * sp2.cols * Sizeof.DOUBLE)
        binOp.op match {
          case AddOp => VecDouble.add(sp1.rows * sp2.cols, result, sp1.pointer, sp2.pointer)
          case MulOp => cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, sp1.rows, sp2.cols, sp1.cols, oneScalar, sp1.pointer, sp1.rows,
            sp2.pointer, sp2.rows, zeroScalar, result, sp1.rows)
        }
        SizedPointer(result, sp1.rows, sp2.cols)
      }
      case binOp: BinaryOperationMatrixScalar => {
        val sp1 = pointerMap.get(binOp.v1).get
        val sp2 = pointerMap.get(binOp.v2).get
        val v2 = Array.ofDim[Double](1)
        cublasGetVector(1, Sizeof.DOUBLE, sp2.pointer, 1, Pointer.to(v2), 1)
        val result = new Pointer()
        cudaMalloc(result, sp1.rows * sp2.cols * Sizeof.DOUBLE)
        binOp.op match {
          case AddOp => VecDouble.addScalar(sp1.rows * sp2.cols, result, sp1.pointer, v2(0))
          case MulOp => VecDouble.mulScalar(sp1.rows * sp2.cols, result, sp1.pointer, v2(0))
        }
        SizedPointer(result, sp1.rows, sp2.cols)
      }
    }
    val r = eval(n)

    val result = Array.ofDim[Double](r.cols * r.rows)
    cublasGetVector(r.cols * r.rows, Sizeof.DOUBLE, r.pointer, 1, Pointer.to(result), 1)

    pointerMap.values.foreach((sp) => cudaFree(sp.pointer))
    cublasDestroy(handle)
    VecFloat.shutdown()

    n match {
      case _: VariableScalar => Scalar(result)
      case _: VariableVector => Vector(result)
      case _: VariableMatrix => Matrix(result, r.rows, r.cols)
      case _: BinaryOperationMatrixScalar => Matrix(result, r.rows, r.cols)
      case _: BinaryOperationMatrixVector => Vector(result)
      case _: BinaryOperationMatrixMatrix => Matrix(result, r.rows, r.cols)
      case _: UnaryOperationMatrix => Matrix(result, r.rows, r.cols)
      case _: UnaryOperationVector => Vector(result)
    }
  }
}