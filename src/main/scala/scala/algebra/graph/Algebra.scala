package scala.algebra.graph

import breeze.linalg.Matrix
import breeze.linalg.Vector
import scala.reflect.ClassTag

trait Algebra[T] {
  trait Evaluator {
    def eval[U <: Tensor[T]](n: Node[U], values: Values): U
  }

  trait Tensor[T]
  case class Scalar[T](data: Array[T]) extends Tensor[T]
  case class Vector[T](data: Array[T]) extends Tensor[T]
  case class Matrix[T](data: Array[T], rows: Int, cols: Int) extends Tensor[T]

  trait Node[U <: Tensor[T]] {
    def eval(values: Values)(implicit evaluator: Evaluator) = evaluator.eval(this, values)
  }

  case class Value[U <: Tensor[T]](k: Variable[U], v: U)
  case class Values(data: Seq[Value[_ <: Tensor[T]]]) {
    def get[U <: Tensor[T]](key: Variable[U]): Option[U] = data.find(value => value.k == key).map(_ match {
      case value: Value[U] => value.v
    })
  }

  case class Constant(value: Scalar[T]) extends Node[Scalar[T]]

  trait Variable[U <: Tensor[T]] extends Node[U] {
    def name: String
    def ->(value: U) = Value[U](this, value)
  }
  case class VariableScalar(override val name: String) extends Variable[Scalar[T]] with ScalarOps
  case class VariableVector(override val name: String) extends Variable[Vector[T]] with VectorOps
  case class VariableMatrix(override val name: String) extends Variable[Matrix[T]] with MatrixOps

  trait ScalarOps {
    self: Node[Scalar[T]] =>

    def +(v: Node[Scalar[T]]) = BinaryOperationScalarScalar(AddOp, ScalarOps.this, v)
    def +(v: Node[Vector[T]]) = BinaryOperationScalarVector(AddOp, ScalarOps.this, v)
    def *(v: Node[Scalar[T]]) = BinaryOperationScalarScalar(MulOp, ScalarOps.this, v)
    def *(v: Node[Vector[T]]) = BinaryOperationScalarVector(MulOp, ScalarOps.this, v)
  }
  trait VectorOps {
    self: Node[Vector[T]] =>

    def +(v: Node[Scalar[T]]) = BinaryOperationVectorScalar(AddOp, VectorOps.this, v)
    def +(v: Node[Vector[T]]) = BinaryOperationVectorVector(AddOp, VectorOps.this, v)
    def *(v: Node[Scalar[T]]) = BinaryOperationVectorScalar(MulOp, VectorOps.this, v)
    //def *(v: Node[Vector[T]]) = BinaryOperationVectorVector(MulOp, self, v)
  }
  trait MatrixOps {
    self: Node[Matrix[T]] =>

    def +(v: Node[Scalar[T]]) = BinaryOperationMatrixScalar(AddOp, MatrixOps.this, v)
    //def +(v: Node[Vector[T]]) = BinaryOperationMatrixVector(AddOp, self, v)
    def +(v: Node[Matrix[T]]) = BinaryOperationMatrixMatrix(AddOp, MatrixOps.this, v)
    def *(v: Node[Scalar[T]]) = BinaryOperationMatrixScalar(MulOp, MatrixOps.this, v)
    def *(v: Node[Vector[T]]) = BinaryOperationMatrixVector(MulOp, MatrixOps.this, v)
    def *(v: Node[Matrix[T]]) = BinaryOperationMatrixMatrix(MulOp, MatrixOps.this, v)
  }

  sealed trait BinaryOperator
  case object AddOp extends BinaryOperator
  case object MulOp extends BinaryOperator
  abstract class BinaryOperation[V1 <: Tensor[T], V2 <: Tensor[T], R <: Tensor[T]](op: BinaryOperator, v1: Node[V1], v2: Node[V2]) extends Node[R]
  abstract class BinaryOperationScalar[V1 <: Tensor[T], V2 <: Tensor[T]](op: BinaryOperator, v1: Node[V1], v2: Node[V2]) extends BinaryOperation[V1, V2, Scalar[T]](op, v1, v2) with ScalarOps
  abstract class BinaryOperationVector[V1 <: Tensor[T], V2 <: Tensor[T]](op: BinaryOperator, v1: Node[V1], v2: Node[V2]) extends BinaryOperation[V1, V2, Vector[T]](op, v1, v2) with VectorOps
  abstract class BinaryOperationMatrix[V1 <: Tensor[T], V2 <: Tensor[T]](op: BinaryOperator, v1: Node[V1], v2: Node[V2]) extends BinaryOperation[V1, V2, Matrix[T]](op, v1, v2) with MatrixOps

  case class BinaryOperationScalarScalar(op: BinaryOperator, v1: Node[Scalar[T]], v2: Node[Scalar[T]]) extends BinaryOperationScalar[Scalar[T], Scalar[T]](op, v1, v2)
  case class BinaryOperationScalarVector(op: BinaryOperator, v1: Node[Scalar[T]], v2: Node[Vector[T]]) extends BinaryOperationVector[Scalar[T], Vector[T]](op, v1, v2)
  case class BinaryOperationVectorScalar(op: BinaryOperator, v1: Node[Vector[T]], v2: Node[Scalar[T]]) extends BinaryOperationVector[Vector[T], Scalar[T]](op, v1, v2)
  case class BinaryOperationVectorVector(op: BinaryOperator, v1: Node[Vector[T]], v2: Node[Vector[T]]) extends BinaryOperationVector[Vector[T], Vector[T]](op, v1, v2)
  case class BinaryOperationMatrixScalar(op: BinaryOperator, v1: Node[Matrix[T]], v2: Node[Scalar[T]]) extends BinaryOperationMatrix[Matrix[T], Scalar[T]](op, v1, v2)
  case class BinaryOperationMatrixVector(op: BinaryOperator, v1: Node[Matrix[T]], v2: Node[Vector[T]]) extends BinaryOperationVector[Matrix[T], Vector[T]](op, v1, v2)
  case class BinaryOperationMatrixMatrix(op: BinaryOperator, v1: Node[Matrix[T]], v2: Node[Matrix[T]]) extends BinaryOperationMatrix[Matrix[T], Matrix[T]](op, v1, v2)

  sealed trait UnaryOperator
  case object SinOp extends UnaryOperator
  case object CosOp extends UnaryOperator
  case object ExpOp extends UnaryOperator
  trait UnaryOperation[U <: Tensor[T]] extends Node[U] {
    def op: UnaryOperator
    def v: Node[U]
  }

  case class UnaryOperationScalar(override val op: UnaryOperator, override val v: Node[Scalar[T]]) extends UnaryOperation[Scalar[T]] with ScalarOps
  case class UnaryOperationVector(override val op: UnaryOperator, override val v: Node[Vector[T]]) extends UnaryOperation[Vector[T]] with VectorOps
  case class UnaryOperationMatrix(override val op: UnaryOperator, override val v: Node[Matrix[T]]) extends UnaryOperation[Matrix[T]] with MatrixOps

  def sin(v: Node[Scalar[T]]) = UnaryOperationScalar(SinOp, v)
  def sin(v: Node[Vector[T]]) = UnaryOperationVector(SinOp, v)
  def sin(v: Node[Matrix[T]]) = UnaryOperationMatrix(SinOp, v)
  def cos(v: Node[Scalar[T]]) = UnaryOperationScalar(CosOp, v)
  def cos(v: Node[Vector[T]]) = UnaryOperationVector(CosOp, v)
  def cos(v: Node[Matrix[T]]) = UnaryOperationMatrix(CosOp, v)
  def exp(v: Node[Scalar[T]]) = UnaryOperationScalar(ExpOp, v)
  def exp(v: Node[Vector[T]]) = UnaryOperationVector(ExpOp, v)
  def exp(v: Node[Matrix[T]]) = UnaryOperationMatrix(ExpOp, v)

}

object AlgebraDouble extends Algebra[Double] {
  def dscalar(name: String) = VariableScalar(name)
  def dvector(name: String) = VariableVector(name)
  def dmatrix(name: String) = VariableMatrix(name)
  def c(v: Double) = Constant(Scalar(Array(v)))
  def values(v: Value[_ <: Tensor[Double]]*) = Values(v)
}
