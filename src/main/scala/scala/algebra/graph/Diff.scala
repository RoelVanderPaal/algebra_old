package scala.algebra.graph

import scala.algebra.graph.AlgebraDouble.Node
import scala.algebra.graph.AlgebraDouble.Tensor
import scala.algebra.graph.AlgebraDouble.Values
import scala.algebra.graph.AlgebraDouble.Variable

trait Diff {
  def diff[U <: Tensor[Double], V <: Tensor[Double]](n: Node[U], variable: Variable[V]): Node[U] = n match {
    case _ => throw new UnsupportedOperationException
  }
}