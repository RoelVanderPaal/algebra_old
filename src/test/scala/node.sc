import breeze.linalg._
import scala.algebra.graph.Node._
import scala.algebra.graph.Evaluator
import scala.algebra.graph.CPUEvaluator

object test {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  implicit object theEvaluator extends CPUEvaluator
  val x = dscalar("x")                            //> x  : scala.algebra.graph.Variable[Double] with algebra.graph.Node.Operations
                                                  //| Scalar = scala.algebra.graph.Node$$anon$1@4b2b7414
  val y = dscalar("y")                            //> y  : scala.algebra.graph.Variable[Double] with algebra.graph.Node.Operations
                                                  //| Scalar = scala.algebra.graph.Node$$anon$1@11d1f39a
  val z = dscalar("z")                            //> z  : scala.algebra.graph.Variable[Double] with algebra.graph.Node.Operations
                                                  //| Scalar = scala.algebra.graph.Node$$anon$1@6ed00c99

  val v = dvector("v")                            //> v  : scala.algebra.graph.Variable[breeze.linalg.DenseVector[Double]] with al
                                                  //| gebra.graph.Node.OperationsVector = scala.algebra.graph.Node$$anon$2@1b2ffe7
                                                  //| 8
  val m = dmatrix("m")                            //> m  : scala.algebra.graph.Variable[breeze.linalg.DenseMatrix[Double]] with al
                                                  //| gebra.graph.Node.OperationsMatrix = scala.algebra.graph.Node$$anon$3@33b93f8
                                                  //| 9

  val vals = values(x -> 2.0d, y -> 3.0d, z -> 4.0d, v -> DenseVector(3, 45), m -> DenseMatrix((3d, 1d), (-1d, -2d)))
                                                  //> vals  : scala.algebra.graph.Values = Values(WrappedArray(Value(scala.algebra
                                                  //| .graph.Node$$anon$1@4b2b7414,2.0), Value(scala.algebra.graph.Node$$anon$1@11
                                                  //| d1f39a,3.0), Value(scala.algebra.graph.Node$$anon$1@6ed00c99,4.0), Value(sca
                                                  //| la.algebra.graph.Node$$anon$2@1b2ffe78,DenseVector(3.0, 45.0)), Value(scala.
                                                  //| algebra.graph.Node$$anon$3@33b93f89,3.0   1.0   
                                                  //| -1.0  -2.0  )))
  x + y + z                                       //> res0: algebra.graph.Node.BinaryOperationScalarScalar = BinaryOperationScalar
                                                  //| Scalar(AddOp(),BinaryOperationScalarScalar(AddOp(),scala.algebra.graph.Node$
                                                  //| $anon$1@4b2b7414,scala.algebra.graph.Node$$anon$1@11d1f39a),scala.algebra.gr
                                                  //| aph.Node$$anon$1@6ed00c99)
  x * y + z                                       //> res1: algebra.graph.Node.BinaryOperationScalarScalar = BinaryOperationScalar
                                                  //| Scalar(AddOp(),BinaryOperationScalarScalar(MulOp(),scala.algebra.graph.Node$
                                                  //| $anon$1@4b2b7414,scala.algebra.graph.Node$$anon$1@11d1f39a),scala.algebra.gr
                                                  //| aph.Node$$anon$1@6ed00c99)
  x + y * z                                       //> res2: algebra.graph.Node.BinaryOperationScalarScalar = BinaryOperationScalar
                                                  //| Scalar(AddOp(),scala.algebra.graph.Node$$anon$1@4b2b7414,BinaryOperationScal
                                                  //| arScalar(MulOp(),scala.algebra.graph.Node$$anon$1@11d1f39a,scala.algebra.gra
                                                  //| ph.Node$$anon$1@6ed00c99))
  v + x                                           //> res3: algebra.graph.Node.BinaryOperationVectorScalar = BinaryOperationVector
                                                  //| Scalar(AddOp(),scala.algebra.graph.Node$$anon$2@1b2ffe78,scala.algebra.graph
                                                  //| .Node$$anon$1@4b2b7414)
  val f = v * x + v                               //> f  : algebra.graph.Node.BinaryOperationVectorVector = BinaryOperationVectorV
                                                  //| ector(AddOp(),BinaryOperationVectorScalar(MulOp(),scala.algebra.graph.Node$$
                                                  //| anon$2@1b2ffe78,scala.algebra.graph.Node$$anon$1@4b2b7414),scala.algebra.gra
                                                  //| ph.Node$$anon$2@1b2ffe78)
  f.eval(vals)                                    //> res4: breeze.linalg.DenseVector[Double] = DenseVector(9.0, 135.0)

  sin(v * x) + y                                  //> res5: algebra.graph.Node.BinaryOperationVectorScalar = BinaryOperationVector
                                                  //| Scalar(AddOp(),UnaryOperationVector(SinOp(),BinaryOperationVectorScalar(MulO
                                                  //| p(),scala.algebra.graph.Node$$anon$2@1b2ffe78,scala.algebra.graph.Node$$anon
                                                  //| $1@4b2b7414)),scala.algebra.graph.Node$$anon$1@11d1f39a)
  val f2 = v + v * x                              //> f2  : algebra.graph.Node.BinaryOperationVectorVector = BinaryOperationVector
                                                  //| Vector(AddOp(),scala.algebra.graph.Node$$anon$2@1b2ffe78,BinaryOperationVect
                                                  //| orScalar(MulOp(),scala.algebra.graph.Node$$anon$2@1b2ffe78,scala.algebra.gra
                                                  //| ph.Node$$anon$1@4b2b7414))
  f2.eval(vals)                                   //> res6: breeze.linalg.DenseVector[Double] = DenseVector(9.0, 135.0)

  (m + x).eval(vals)                              //> res7: breeze.linalg.DenseMatrix[Double] = 5.0  3.0  
                                                  //| 1.0  0.0  
  sin(m + x).eval(vals)                           //> res8: breeze.linalg.DenseMatrix[Double] = -0.9589242746631385  0.14112000805
                                                  //| 98672  
                                                  //| 0.8414709848078965   0.0                 
  sin(x).eval(vals)                               //> res9: Double = 0.9092974268256817
}