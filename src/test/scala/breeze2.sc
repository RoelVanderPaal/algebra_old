import breeze.linalg.DenseVector
import breeze.linalg.DenseMatrix
import breeze.linalg.Matrix

object breeze2 {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  import scala.algebra.tensor.MatrixFunction._

  val v = DenseVector.fill(2) { 5.0 }             //> v  : breeze.linalg.DenseVector[Double] = DenseVector(5.0, 5.0)
  val u = DenseVector.fill(2) { 6.0 }             //> u  : breeze.linalg.DenseVector[Double] = DenseVector(6.0, 6.0)
  myMethod(v, u)                                  //> res0: breeze.linalg.DenseVector[Double] = DenseVector(11.0, 11.0)

  val a = DenseMatrix((100.0, 2.0), (3.0, 4.0))   //> a  : breeze.linalg.DenseMatrix[Double] = 100.0  2.0  
                                                  //| 3.0    4.0  

  //  myMethod2(a, 2.1)
  //  myMethod2(a, 2.0)
  //  myMethod2(v, 2.0)
  myMethod2(a, a)                                 //> res1: from = 200.0  4.0  
                                                  //| 6.0    8.0  
  
  

}