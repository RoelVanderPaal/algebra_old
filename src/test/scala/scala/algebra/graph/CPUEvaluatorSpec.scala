package scala.algebra.graph

import org.scalatest.Matchers
import org.scalatest.FunSpec

class CPUEvaluatorSpec extends FunSpec with Matchers with EvaluatorBehaviors {
  def evaluator = new CPUEvaluator

  describe("A CPUEvaluator") {
    it should behave like evaluator(evaluator)
  }

}