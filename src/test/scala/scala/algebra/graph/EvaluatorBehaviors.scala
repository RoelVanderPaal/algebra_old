package scala.algebra.graph

import org.scalatest.FunSpec
import scala.algebra.graph.AlgebraDouble._
import org.scalatest.Matchers
import breeze.linalg.DenseMatrix
import org.scalatest.matchers.Matcher
import org.scalatest.matchers.MatchResult
import org.scalautils.Prettifier

trait EvaluatorBehaviors {
  this: FunSpec with Matchers =>

  val x = dscalar("x")
  val x_val = Scalar(Array(2d))
  val y = dscalar("y")
  val v = dvector("v")
  val v_val = Vector(Array[Double](3, 45))
  val m = dmatrix("m")
  val m_val = Matrix(Array[Double](3, 1, -1, -2), 2, 2)
  val n = dmatrix("n")
  val n_val = Matrix(Array[Double](3d, 1d, 4d, -1d, -2d, 5d), 2, 3)

  val vals = values(x -> x_val, y -> Scalar(Array(3)), v -> v_val, m -> m_val)

  implicit val tolerance = 1e-7

  def evaluator(evaluator: Evaluator) {
    describe("when evaluated") {
      it("should handle variables") {
        evaluator.eval(x, vals).data should be(x_val.data)
        evaluator.eval(v, vals).data should be(v_val.data)
        compare(evaluator.eval(m, vals), m_val)
      }
      it("should handle unaryOps") {
        evaluator.eval(sin(v), vals).data should equalWithTolerance(breeze.numerics.sin(v_val.data))
        evaluator.eval(cos(v), vals).data should equalWithTolerance(breeze.numerics.cos(v_val.data))
        evaluator.eval(exp(v), vals).data should equalWithTolerance(breeze.numerics.exp(v_val.data))
        evaluator.eval(sin(m), vals).data should equalWithTolerance(breeze.numerics.sin(m_val.data))
        evaluator.eval(cos(m), vals).data should equalWithTolerance(breeze.numerics.cos(m_val.data))
        evaluator.eval(exp(m), vals).data should equalWithTolerance(breeze.numerics.exp(m_val.data))
      }
      it("should handle binaryOps") {
//        evaluator.eval((x + v), vals).data.shouldBe(Array(5d, 47d))
        evaluator.eval((m * v), vals).data should equalWithTolerance(Array(-36, -87))
        evaluator.eval((m * m), vals).data should equalWithTolerance(Array(8, 1, -1, 3))
        evaluator.eval(m + m, vals).data should equalWithTolerance(Array(6, 2, -2, -4))
        evaluator.eval(m + x, vals).data should equalWithTolerance(Array(5, 3, 1, 0))
        evaluator.eval(m * x, vals).data should equalWithTolerance(Array(6, 2, -2, -4))
      }
      it("should combine unaryOps and binaryOps") {
        evaluator.eval(sin(m * m), vals).data should equalWithTolerance(breeze.numerics.sin(Array[Double](8, 1, -1, 3)))
        evaluator.eval(sin(cos(m)), vals).data should equalWithTolerance(breeze.numerics.sin(breeze.numerics.cos(m_val.data)))
      }
    }
  }

  def compare(m: Matrix[Double], expected: Matrix[Double]) = {
    m.data should equalWithTolerance(expected.data)
    m.rows should be(expected.rows)
    m.cols should be(expected.cols)
  }

  def equalWithTolerance(right: Array[Double])(implicit tolerance: Double) =
    Matcher { (left: Array[Double]) =>
      MatchResult((left zip right) forall {
        case (a, b) => {
          import Math.abs
          abs(a) <= abs(b * (1 + tolerance)) && abs(a) >= abs(b * (1 - tolerance))
        }
      },
        pretty(left) + " did not equal " + pretty(right) + " with tolerance " + tolerance,
        pretty(left) + " equaled " + pretty(right) + " with tolerance " + tolerance)
    }

  def pretty(a: Array[Double]) = Prettifier.default(a)

  def toMatrix(d: DenseMatrix[Double]): Matrix[Double] = Matrix(d.data, d.rows, d.cols)
}