package scala.algebra.graph

import scala.algebra.graph.AlgebraDouble._
import org.scalatest.FunSpec
import org.scalatest.Matchers
import breeze.linalg.DenseMatrix
import breeze.linalg.DenseVector
import scala.util.Random

class GPUEvaluatorSpec extends FunSpec with Matchers with EvaluatorBehaviors {
  def evaluator = new GPUEvaluator

  describe("A GPUEvaluator") {
    it should behave like evaluator(evaluator)
  }

}