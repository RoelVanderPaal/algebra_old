import breeze.linalg._
import breeze.numerics._
object breezeTest {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val x = DenseVector.zeros[Double](5)            //> x  : breeze.linalg.DenseVector[Double] = DenseVector(0.0, 0.0, 0.0, 0.0, 0.0
                                                  //| )

  val a: DenseMatrix[Double] = DenseMatrix((100.0, 2.0), (3.0, 4.0))
                                                  //> a  : breeze.linalg.DenseMatrix[Double] = 100.0  2.0  
                                                  //| 3.0    4.0  
  val a2: DenseMatrix[Double] = DenseMatrix((100.0, 2.0, 3.0), (3.0, 4.0, 5.0))
                                                  //> a2  : breeze.linalg.DenseMatrix[Double] = 100.0  2.0  3.0  
                                                  //| 3.0    4.0  5.0  
  val b = DenseVector.fill(2) { 5.0 }             //> b  : breeze.linalg.DenseVector[Double] = DenseVector(5.0, 5.0)
  val c = DenseVector.fill(2) { 6.0 }             //> c  : breeze.linalg.DenseVector[Double] = DenseVector(6.0, 6.0)
  a.data                                          //> res0: Array[Double] = Array(100.0, 3.0, 2.0, 4.0)
  sum(a)                                          //> res1: Double = 109.0
  a + a                                           //> res2: breeze.linalg.DenseMatrix[Double] = 200.0  4.0  
                                                  //| 6.0    8.0  
  a + 2.0                                         //> res3: breeze.linalg.DenseMatrix[Double] = 102.0  4.0  
                                                  //| 5.0    6.0  
  a * 2.0                                         //> res4: breeze.linalg.DenseMatrix[Double] = 200.0  4.0  
                                                  //| 6.0    8.0  
  a - a                                           //> res5: breeze.linalg.DenseMatrix[Double] = 0.0  0.0  
                                                  //| 0.0  0.0  
  a * a                                           //> res6: breeze.linalg.DenseMatrix[Double] = 10006.0  208.0  
                                                  //| 312.0    22.0   
  a * a2                                          //> res7: breeze.linalg.DenseMatrix[Double] = 10006.0  208.0  310.0  
                                                  //| 312.0    22.0   29.0   
  -a                                              //> res8: breeze.linalg.DenseMatrix[Double] = -100.0  -2.0  
                                                  //| -3.0    -4.0  
  b * 2.0                                         //> res9: breeze.linalg.DenseVector[Double] = DenseVector(10.0, 10.0)

  exp(b)                                          //> res10: breeze.linalg.DenseVector[Double] = DenseVector(148.4131591025766, 14
                                                  //| 8.4131591025766)
  sigmoid(a * (c - b))                            //> res11: breeze.linalg.DenseVector[Double] = DenseVector(1.0, 0.99908894880559
                                                  //| 94)
  val d = DenseVector(20, 30, 100)                //> d  : breeze.linalg.DenseVector[Int] = DenseVector(20, 30, 100)
  d.argmax                                        //> res12: Int = 2

  a.cols                                          //> res13: Int = 2
  a.rows                                          //> res14: Int = 2
  sin(a)                                          //> res15: breeze.linalg.DenseMatrix[Double] = -0.5063656411097588  0.9092974268
                                                  //| 256817   
                                                  //| 0.1411200080598672   -0.7568024953079282  
  b dot c                                         //> res16: Double = 60.0
}